"""
File to store all the configuration information
"""

# Redshift Credentials

redshift_user = "ap-admin"
redshift_password = "Apadmin123!"
redshift_cluster = "redshift-adventure-park-dw-cluster.cvhfi9cmomso.ap-southeast-2"
port = "5439"
redshift_database = "dw-adventurepark"

connstr = (
    "redshift+psycopg2://"
    + redshift_user
    + ":"
    + redshift_password
    + "@"
    + redshift_cluster
    + ".redshift.amazonaws.com:"
    + port
    + "/"
    + redshift_database
)

# Kounta Credentials

kounta_client_id = "mlk0E0riUOe9ERWk"
kounta_client_secret = "FgnlRQ66Ti0OEa4lq2dvA1j4CkyLwlJvUWnPmavM"
kounta_redirect_url = "https://my.kounta.com/authorize/"
kounta_refresh_token = "e9cac61bee178681d16a53a3a3a926517c4a3f24"

kounta_dict = {
    "salesdata_time": {"Column_Name": "Date"},
    "location_site_name": {"Column_Name": "Outlet"},
    "productname": {"Column_Name": "Product"},
    "product_pos_categoryname": {"Column_Name": "Category"},
    "sales_amount_sold": {"Column_Name": "Amount_Sold"},
    "sales_product_qty": {"Column_Name": "Quantity"},
    "sales_total_ex_tax": {"Column_Name": "Revenue"},
    "sales_cost_ex_tax": {"Column_Name": "Cost"},
}

# Electricity config dictionary
electricity_dict = {
    "energy_peak_rate": 8.2804,
    "energy_off_peak_rate": 4.5716,
    "regulated_charge": 0.0368 + 0.041,
    "aemo_frc": 0.3643 / (24 * 2),
    "environmental_charge": (3.717 * 0.1726) + (4 * 0.288) + (3.15 * 0.1854),
    "metering_charge": 3.452055 / (24 * 2),
    "retail_charge": 20 / (30 * 24 * 2),
}


# Kronos Credentials
sal_offset = 3600
casual_rate = 21.97
