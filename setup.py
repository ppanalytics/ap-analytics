"""
This file is for setting up the global variables used throughout the project
Customized folder paths and details to be initialized here
"""

import pandas as pd
import datetime
import time

Start_time = datetime.datetime.now().replace(microsecond=0)
print("Reading Started: ", Start_time)

### REVENUE DATA ###

# df_revenue = pd.read_csv('data/Adventure Park_Revenue Report_2019_2020.csv')
# df_attn = pd.read_csv('data/Adventure Park_Attendance_2019_2020.csv')
# df_book = pd.read_csv('data/Adventure Park_Bookings_200820201332_1018710327.csv')
# cost_price_third_party = pd.read_excel('data/third_party_agreements.xls', sheet_name='agreements',
#                                       index_col='ParentProductName')

df_rev = pd.read_csv("data/Adventure Park_Revenue Report_200820201334_948152523.csv")
df_attn = pd.read_csv("data/Adventure Park_Attendance_200820201416_1239014084.csv")
df_book = pd.read_csv("data/Adventure Park_Bookings_200820201332_1018710327.csv")
third_party_cost_table = pd.read_excel(
    "data/third_party_agreements.xls",
    sheet_name="agreements",
    index_col="ParentProductName",
)
df_fb = pd.read_csv("data/food and bev/insights salelines 2020-11-17T2209.csv")
df_fb_cols = pd.read_excel(
    "data/food and bev/Food_and_Bev_Info.xlsx", sheet_name="Kounta_Columns", index_col=0
).to_dict("index")
for i in df_fb_cols.keys():
    df_fb = df_fb.rename(columns={i: df_fb_cols[i]["Column_Name"]})

### COST DATA ###

# Electricity Information
# cols =["Date"]
# for hour in range(1,25):
#    cols.append("Hour_"+ str(hour))
hour_list = lambda x: ["Hour_" + str(i) for i in range(1, x)]
cols = ["Date"] + (hour_list(25))

df_park_usage = pd.read_csv(
    "data/utility/Park_Usage_Dataset_01Jan19_10Nov20.csv", skiprows=8, names=cols
).assign(Date=lambda x: pd.to_datetime(x.Date))
df_office_usage = pd.read_csv(
    "data/utility/Office_Usage_Dataset_01Jan19_10Nov20.csv", skiprows=8, names=cols
).assign(Date=lambda x: pd.to_datetime(x.Date))
df_rate = (
    pd.read_csv(
        "data/utility/PVExport-AA-20201029-171455-08ZD6X3F8.csv", skiprows=3, names=cols
    )
    .iloc[1:]
    .assign(Date=lambda x: pd.to_datetime(x.Date))
)

# Wages Information
df_wages = pd.read_excel(
    "data/Complete 19-20 Time Wage Master Sheet.xlsx", sheet_name="Hours Worked Log"
)
sal_offset = 3600

End_time = datetime.datetime.now().replace(microsecond=0)
print("Reading Finished: ", End_time)
print("Time Taken", End_time - Start_time)

# utility_data = pd.read_csv('data/utility/.csv')
