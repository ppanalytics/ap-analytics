import requests
import numpy as np
import pandas as pd
import webbrowser
import json
import datetime as dt

import boto3
from io import StringIO
from datetime import date, datetime, timedelta

CLIENT_ID = "2vbJo2EjbGMxFlY2yiauX3Msl20Xq17n"
CLIENT_SECRET = "KU_7_m-Mfz7CXy-pqUDhPo8kNEem9HNSYS4ohmdtPU9XLEVUPfKQuW7VHQoE4Nrv"
API_URL = "https://api.roller.app"


# file_name = 'roller/testing_writing_to_private_s3_bucket.csv'
# bucket_name = 'adventure-park'


def RollerAuth():
    # Get Access Token
    url = "https://api.roller.app/token"
    body = {"client_id": "" + CLIENT_ID + "", "client_secret": "" + CLIENT_SECRET + ""}
    headers = {
        "Content-Type": "application/json",
        "charset": "UTF-8",
        "Accept": "application/json",
    }

    resp = requests.post(url, json=body, headers=headers)

    if resp.status_code != 200:
        # if something went wrong.
        print("Error Code : ", resp.status_code)
    else:
        json_response = resp.json()
        return json_response["access_token"]


def RollerRequest(url, access_token, page, start_date, end_date):
    response = requests.get(
        API_URL
        + "/data/"
        + url
        + "?pageNumber="
        + str(page)
        + "&startDate="
        + start_date
        + "&endDate="
        + end_date
        + "",
        headers={
            "Authorization": "Bearer " + access_token,
            "Accept": "application/json",
        },
    )

    json_response = response.json()
    return json_response


def get_first_sunday_october(year):
    dt = date(year, 10, 1)
    dt += timedelta(days=6 - dt.weekday())

    return dt


def get_first_sunday_april(year):
    dt = date(year, 4, 1)
    dt += timedelta(days=6 - dt.weekday())

    return dt


# def _write_dataframe_to_csv_on_s3(dataframe, filename):
#    """ Write a dataframe to a CSV on S3 """
#    print("Writing {} records to {}".format(len(dataframe), filename))
#     # Create buffer
#    csv_buffer = StringIO()
#    # Write dataframe to buffer
#    dataframe.to_csv(csv_buffer, sep="|", index=False)
#    # Create S3 object
#    s3_resource = boto3.resource("s3")
#    # Write buffer to S3 object
#    s3_resource.Object(bucket_name, filename).put(Body=csv_buffer.getvalue())

access_token = RollerAuth()

# enter urls here
api_urls = ["bookingitems"]
df_api_urls = pd.DataFrame.from_dict(api_urls)

for m in range(len(df_api_urls)):

    tmp_df = df_api_urls.iloc[m][0]

    STRART_DATE = "2021-07-01"
    END_DATE = "2021-07-07"

    # Set Start End dates
    start = dt.datetime.strptime(STRART_DATE, "%Y-%m-%d")
    end = dt.datetime.strptime(END_DATE, "%Y-%m-%d")
    return_jsons = []

    while start < end:

        current_page = 1
        total_pages = 2

        start_date = dt.datetime.strftime(start, "%Y-%m-%d")
        end_date = dt.datetime.strftime(start + dt.timedelta(days=1), "%Y-%m-%d")

        if tmp_df == "products":
            start = end

        while total_pages >= current_page:
            return_json = RollerRequest(
                tmp_df, access_token, current_page, start_date, end_date
            )
            # print(return_json)
            total_pages = return_json["totalPages"]
            return_jsons.append(return_json)
            current_page += 1

        start += dt.timedelta(days=1)

    dataframe = pd.DataFrame.from_dict(return_jsons)
    test_df = dataframe["items"]

    roller_output = pd.DataFrame()

    for i in range(len(test_df)):

        try:

            temp_df = pd.DataFrame(test_df[i])

            # create new column with type DateTime
            temp_df["bookingModifiedDateNew"] = pd.to_datetime(
                temp_df["bookingModifiedDate"]
            )
            # change the format of the date
            temp_df["bookingModifiedDateNew"] = pd.to_datetime(
                temp_df["bookingModifiedDateNew"].dt.strftime("%Y-%m-%d %H:%M:%S")
            )
            # get the current item's date
            current_row_year = temp_df["bookingModifiedDateNew"][0].year

            # get first sunday of october and april to detect if the date falls into AEST or AEDT
            first_sunday_oct = get_first_sunday_october(current_row_year)
            first_sunday_apr = get_first_sunday_april(current_row_year)

            if (
                    first_sunday_apr <= temp_df["bookingModifiedDateNew"][0] <= first_sunday_oct
            ):

                # convert the UTC column to AEST
                temp_df["bookingModifiedDateNewAU"] = (
                    temp_df["bookingModifiedDateNew"]
                    .dt.tz_localize("UTC")
                    .dt.tz_convert("Australia/Melbourne")
                )
                # since AEDT should be the correct format because the date falls in between oct(ly) and apr(cy)
                temp_df["bookingModifiedDateNewAU"] = temp_df[
                    "bookingModifiedDateNew"
                ] - timedelta(hours=1)
                # split the date only and create a new column
                temp_df["bookingModifiedDateNewAUDateOnly"] = pd.to_datetime(
                    temp_df["bookingModifiedDateNewAU"].dt.strftime("%Y-%m-%d")
                )

            else:
                # convert the UTC column to AEST
                temp_df["bookingModifiedDateNewAU"] = (
                    temp_df["bookingModifiedDateNew"]
                    .dt.tz_localize("UTC")
                    .dt.tz_convert("Australia/Melbourne")
                )
                # split the date only and create a new column
                temp_df["bookingModifiedDateNewAUDateOnly"] = pd.to_datetime(
                    temp_df["bookingModifiedDateNewAU"].dt.strftime("%Y-%m-%d")
                )

        except ValueError and KeyError:
            continue

        roller_output = pd.concat(
            [roller_output, temp_df], axis=0, ignore_index=True, sort=True
        )
