import numpy as np
import pandas as pd
import requests
from bs4 import BeautifulSoup
import json
import re

API_KEY = "dbfgrhbcrd3kg16cm1ktn55iixlmm1t2"
USER_NAME = "API.USER"
PASSWORD = "Kronos%2A123"
COMPANY = "6108042"  # company short name

URL = "https://secure.workforceready.com.au/ta/rest/"
URI = "v1/report/saved/19177183"


def GetToken():
    headers = {"Api-Key": API_KEY, "Content-Type": "application/json"}
    data = {
        "credentials": {"username": USER_NAME, "password": PASSWORD, "company": COMPANY}
    }
    r = requests.post(
        URL + "v1/login", headers=headers, data=json.dumps(data), verify=False
    )
    BearerToken = json.loads(r.content)["token"]
    return BearerToken


def RunReport(token, URI_, ID):
    headers = {"Authentication": "Bearer " + token, "Accept": "application/xml"}
    r = requests.get(URL + URI_ + ID, headers=headers, verify=False)
    return r


token = GetToken()

data = RunReport(token, URI, "").text


def get_kronos_df(employees):
    header_list = []
    body_list = []

    try:
        soup = BeautifulSoup(employees, "html.parser")
        authors = soup.header.find_all("label")

        for i in soup.header.find_all("label"):
            # print(i.get_text())
            header_list.append(i.get_text())

        for i in soup.body.find_all("row"):
            a = i.get_text().replace("     ", "")
            # print(a)
            list1 = []
            for j in a.split("\n")[:-1]:
                list1.append(j.strip())
            body_list.append(list1)
            df = pd.DataFrame(body_list, columns=header_list)
    except ValueError:
        df = pd.Dataframe(body_list)

    return df


df_kronos = get_kronos_df(data)

print(df_kronos)
