import json
import requests
import webbrowser
import base64
import pandas as pd
import datetime
from csv import reader
import re
import base64

pd.set_option("display.max_columns", None)
pd.options.display.float_format = "{:.2f}".format

from config import (
    kounta_client_id,
    kounta_client_secret,
    kounta_redirect_url,
    kounta_refresh_token,
)


# def get_token(client_id, redirect_url, b64_id_secret):
#     # 1. Send a user to authorize your app
#     auth_url = (
#         """https://my.kounta.com/authorize/?"""
#         + """response_type=code"""
#         + """&client_id="""
#         + client_id
#         + """&redirect_uri="""
#         + redirect_url
#         + """&state=123"""
#     )
#     webbrowser.open_new(auth_url)
#     # 2. Users are redirected back to you with a code
#     auth_code = input("What is the Code? ")
#     # 3. Exchange the code
#     exchange_code_url = "https://api.kounta.com/v1/token.json"
#     response = requests.post(
#         exchange_code_url,
#         headers={"Authorization": "Basic " + b64_id_secret},
#         data={
#             "grant_type": "authorization_code",
#             "code": auth_code,
#             "redirect_uri": redirect_url,
#         },
#     )
#     json_response = response.json()
#     # 4. Receive your tokens
#     return json_response


def get_access_token(refresh_token, b64_id_secret):
    token_refresh_url = "https://api.kounta.com/v1/token.json"
    response = requests.post(
        token_refresh_url,
        headers={
            "Authorization": "Basic " + b64_id_secret,
            "Content-Type": "application/x-www-form-urlencoded",
        },
        data={"grant_type": "refresh_token", "refresh_token": refresh_token},
    )
    # json to python dict
    json_response = response.json()
    return json_response


def get_data(access_token, uri):
    connections_url = "https://api.kounta.com" + uri + ""
    response = requests.get(
        connections_url,
        headers={
            "Authorization": "Bearer " + access_token,
            "Content-Type": "application/json",
        },
    )
    json_response = response.json()

    return json.dumps(json_response, indent=4, sort_keys=True)


def format_data(token, url):
    url_json = get_data(token, url)
    data = json.loads(url_json)
    return pd.json_normalize(data, max_level=1)


def execute():
    b64_id_secret = base64.b64encode(
        bytes(kounta_client_id + ":" + kounta_client_secret, "utf-8")
    ).decode("utf-8")

    response = get_access_token(kounta_refresh_token, b64_id_secret)

    # extracting the access token from the response
    access_token = response["access_token"]

    # Each product will belong to one or more categories.
    url_categories = '/v1/companies/11257/categories.json'

    data = format_data(access_token, url_categories)
    print(data)


execute()
