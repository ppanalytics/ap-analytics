import pandas as pd
from sqlalchemy import create_engine
import datetime as dt
import time

from config import connstr, kounta_dict, casual_rate

engine = create_engine(connstr)

with engine.connect() as conn, conn.begin():
    df_kronos = pd.read_sql(
        """
           select
             *
           from kronos.employees;""",
        conn,
    )

    df_park_usage = pd.read_sql(
        """
              select
                *
              from origin.park_usage;""",
        conn,
    )

    df_office_usage = pd.read_sql(
        """
              select
                *
              from origin.office_usage;""",
        conn,
    )

    # df_bookings = pd.read_sql(
    #     """
    #           select
    #             *
    #           from roller.bookingitems;""",
    #     conn,
    # )
    #
    df_rev = pd.read_sql(
        """
              select
                *
              from roller.revenues;""",
        conn,
    )

    df_attn = pd.read_sql(
        """
              select
                *
              from roller.attendances;""",
        conn,
    )

    df_products = pd.read_sql(
        """
              select
                *
              from roller.products;""",
        conn,
    )

    df_kounta = pd.read_sql(
        """
              select
                *
              from kounta.foodandbev;""",
        conn,
    )


# formatting the data as per requirements

# roller data processing
def datetime_from_utc_to_local(utc_datetime):
    now_timestamp = time.time()
    offset = dt.datetime.fromtimestamp(now_timestamp) - dt.datetime.utcfromtimestamp(
        now_timestamp
    )
    return utc_datetime + offset


df_products = df_products[df_products["productstatus"] != "Archived"]
df_rev = df_rev.merge(df_products, how="left", on="productid")
df_attn["checkindatetime"] = df_attn["checkindatetime"].apply(
    lambda x: datetime_from_utc_to_local(pd.to_datetime(x))
)
# to create the ticketid in attendance data, need to concat bookingreference-bookingitempartid
df_attn["ticketid"] = df_attn.apply(
    lambda x: str(x["bookingreference"]) + "-" + str(x["bookingitempartid"]), axis=1
)


# kounta data processing
def convert_kounta_to_dollar(mystr):
    mylist = mystr.split("$")

    if len(mylist[0]):
        return -1 * float(mylist[1].split(")")[0])
    else:
        return float(mylist[1])


for i in kounta_dict.keys():
    df_kounta = df_kounta.rename(columns={i: kounta_dict[i]["Column_Name"]})

df_kounta["Revenue"] = df_kounta["Revenue"].apply(convert_kounta_to_dollar)
df_kounta["Cost"] = df_kounta["Cost"].apply(convert_kounta_to_dollar)


# kronos data processing
def calculate_salary(row):
    if len(row["rates1"]):
        return casual_rate * row["calchours"]
    else:
        return 0


df_kronos["Total_Wage_Including_Penalty_Rates"] = df_kronos.apply(
    calculate_salary, axis=1
)
