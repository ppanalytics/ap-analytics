"""
This file looks at the labour and utility information
"""

import pandas as pd
import numpy as np
from data_ingestor import df_park_usage, df_office_usage


def locationCost(df1, df2, locationType):
    """
    Both Dataframes need a Date column

    :param df1: df1 is the rate
    :type df1:
    :param df2: df2 is the location cost
    :type df2:
    :param locationType: loactionType is the Name for the location
    :type locationType:
    :return:
    :rtype:
    """

    # Two Dataframes with Overlapping Dates
    df1 = df1.merge(df2["Date"], how="inner").sort_values(by=("Date"))
    df2 = df2.merge(df1["Date"], how="inner").sort_values(by=("Date"))

    # Combine Hours
    df_combined = (
        pd.concat([df2.drop(["Date"], axis=1), df1.drop(["Date"], axis=1)], axis=1)
        .groupby(level=0, axis=1)
        .prod()
    )
    df_combined = pd.concat([df2.filter(["Date"]), df_combined], axis=1)
    df_combined = df_combined.fillna(0)
    df_combined[locationType] = df_combined.sum(axis=1)
    df_combined = df_combined.filter(["Date", locationType])

    return df_combined


def utilityCost(**kwargs):
    """
    df_rate is the hour date electricity price in $/kWh
    df_office is the office usage in kWh
    df_park is the park usage in kWh
    """

    df_rate = kwargs.get("rates", None)
    df_office = kwargs.get("office", None)
    df_park = kwargs.get("park", None)

    # Get each location's costs
    df_office = locationCost(df_rate, df_office, "Office_Cost")
    df_park = locationCost(df_rate, df_park, "Park_Cost")

    # Combine - need to check for overlapping or add 0's
    df_utility = df_office.merge(df_park, how="outer").fillna(0)

    return df_utility


def wagesCost(df_wages, sal_offset):
    """

    :param df_wages: dates and wage amounts
    :type df_wages:
    :param sal_offset: amount to put for full time workers
    :type sal_offset:
    :return:
    :rtype:
    """
    df_wages = (
        df_wages.groupby("date")
        .agg({"employeeid": ["count"], "Total_Wage_Including_Penalty_Rates": ["sum"]})
        .reset_index()
        .assign(
            Total_Wage_Including_Penalty_Rates=lambda x: x.Total_Wage_Including_Penalty_Rates
            + sal_offset
        )
    )

    df_wages.columns = ["dates", "count", "expense"]
    df_wages["expense_type"] = "Employee Wage"

    return df_wages


def foodCost(df):
    """

    :param df:
    :type df:
    :return:
    :rtype:
    """
    df = df.assign(Date=lambda x: pd.to_datetime(x.Date, format="%d/%m/%Y %H:%M"))
    df = df.assign(Date=df["Date"].dt.date)

    df_agg = (
        df.groupby(["Date", "Outlet"])
        .agg({"Cost": ["sum"], "Quantity": ["sum"]})
        .reset_index()
    )

    df_agg.columns = ["Date", "Outlet", "Cost", "Quantity"]
    return df_agg