'''
This file is to attribute ticket revenue based on different revenue buckets
'''

import pandas as pd
import numpy as np
from collections import defaultdict
import re
import datetime as dt
import time

from setup import df_revenue, df_attn, cost_price_third_party


def day_revenue(df, revenue_type):
    '''
    BookingPayment Id is 0
    ReceiptNumber is under which the order is stored
    NetRevenue is considered as the tax isn't included
    EntryType is Redemption
    PaymentStatus can be Partially Paid or Fully Paid. Pendings are usually refunded
    '''

    mydf = df[(df['EntryType'] == 'Redemption') & (df['ProductName'] != 'RACV Height upgrade')
              & ((df['PaymentStatus'] == 'Partially Paid') | (df['PaymentStatus'] == 'Fully Paid'))].copy()
    mydf['timestamp'] = mydf['RedemptionDate'].apply(lambda x: pd.to_datetime(str(x)))
    mydf['dates'] = mydf['timestamp'].dt.date

    rdf = mydf.groupby('dates').agg({'TicketId': ['count'], 'NetRevenue': ['sum']}).reset_index()
    rdf.columns = ['dates', 'count', 'revenue']
    rdf['revenue_type'] = revenue_type

    return rdf


def season_admit(admit_frequency, ticket_no, net_revenue):
    entry_num = len(admit_frequency[ticket_no])
    realized_revenue = net_revenue / entry_num
    return pd.DataFrame([[day, ticket_no, realized_revenue] for day in admit_frequency[ticket_no]])


def season_revenue(df, admit_frequency, revenue_type):
    '''
    ReceiptNumber is under which the order is stored
    NetRevenue is considered as the tax isn't included
    EntryType is Redemption
    PaymentStatus can be Partially Paid or Fully Paid. Pendings are usually refunded
    The revenue is recognised across multiple days equally from admit_frequency
    '''

    rdf = pd.DataFrame()
    mydf = df[(df['EntryType'] == 'Redemption') & (
            (df['PaymentStatus'] == 'Partially Paid') | (df['PaymentStatus'] == 'Fully Paid'))].copy()

    for idx, row in mydf.iterrows():
        # have not catered for scenarios that do not have a ticket id in Attendance data
        if len(admit_frequency[row['TicketId']]):
            rdf = rdf.append(season_admit(admit_frequency, row['TicketId'], row['NetRevenue']))

    rdf.columns = ['dates', 'tickets', 'revenue']
    tdf = rdf.groupby('dates').agg({'tickets': ['count'], 'revenue': ['sum']}).reset_index()
    tdf.columns = ['dates', 'count', 'revenue']
    tdf['revenue_type'] = revenue_type

    return tdf, rdf


def group_revenue(df):
    '''
    BookingPayment Id is 0
    ReceiptNumber is under which the order is stored
    NetRevenue is considered as the tax isn't included
    EntryType is Redemption
    PaymentStatus can be Partially Paid or Fully Paid. Pendings are usually refunded
    Catering is not considered, only admissions per person
    Assumption - person in each group enters only once
    '''

    mydf = df[(df['EntryType'] == 'Redemption') & (
            (df['PaymentStatus'] == 'Partially Paid') | (df['PaymentStatus'] == 'Fully Paid'))].copy()
    mydf['timestamp'] = mydf['RedemptionDate'].apply(lambda x: pd.to_datetime(str(x)))
    mydf['dates'] = mydf['timestamp'].dt.date

    rdf = mydf.groupby('dates').agg({'TicketId': ['count'], 'NetRevenue': ['sum']}).reset_index()
    rdf.columns = ['dates', 'count', 'revenue']
    rdf['revenue_type'] = 'Group Tickets'

    return rdf


def xmas_calculator(row, xmas_booking):
    if (len(set(xmas_booking[row['ReceiptNumber']])) > 1) or (
            row['ParentProductName'] == '2019/2020 Season Pass Discounted Festival of Lights Ticket'):
        # respective discounted applied to net revenue
        return row['NetRevenue']

    elif row['ParentProductName'] == '2019/2020 Christmas Festival of Lights':
        if (row['NetRevenue'] + row['TaxPayable'] + row['DeferredRevenue']) == 0:
            return row['NetRevenue']
        else:
            # 10% is gone to tax
            return (row['NetRevenue'] + row['TaxPayable'] + row['DeferredRevenue']) * 0.9


def christmas_revenue(df, xmas_booking):
    '''
    BookingPayment Id is 0
    ReceiptNumber is under which the order is stored
    NetRevenue is considered as the tax isn't included
    EntryType is Redemption
    PaymentStatus can be Partially Paid or Fully Paid. Pendings are usually refunded

    # season pass members get 50% off (445579), first night they get 100% off (not sure)
    # - identify using deferred revenue. if the net + tax + deferred = 0 then no discount
    # - otherswise 2019/2020 Season Pass Discounted Festival of Lights Ticket already applies the discount
    # under 90 cms child is free (454630) should be accounted for, redemption
    # - if revenue is 0 and ticket is redeemed
    '''

    mydf = df[(df['EntryType'] == 'Redemption') & (
            (df['PaymentStatus'] == 'Partially Paid') | (df['PaymentStatus'] == 'Fully Paid'))].copy()
    mydf['timestamp'] = mydf['RedemptionDate'].apply(lambda x: pd.to_datetime(str(x)))
    mydf['dates'] = mydf['timestamp'].dt.date

    mydf['CalculatedRevenue'] = mydf.apply(lambda x: xmas_calculator(x, xmas_booking), axis=1)

    rdf = mydf.groupby('dates').agg({'TicketId': ['count'], 'CalculatedRevenue': ['sum']}).reset_index()
    rdf.columns = ['dates', 'count', 'revenue']
    rdf['revenue_type'] = 'Christmas Night Lights Tickets'

    return rdf, mydf


def party_calculator(row, cost_price_third_party, w1, w2, total_racv_admits):
    if (row['ParentProductName'] == '2019/2020 RACV'):
        return (w1 / total_racv_admits) * float(cost_price_third_party.loc['RACV']['Junior Admission']
                                                ) + (w2 / total_racv_admits) * float(
            cost_price_third_party.loc['RACV']['General Admission'])

    elif row['ProductName'] == 'RACV Height upgrade':
        return row['NetRevenue']


def party_revenue(df, cost_price_third_party, type_admits):
    '''
    BookingPayment Id is 0
    ReceiptNumber is under which the order is stored
    NetRevenue is considered as the tax isn't included
    EntryType is Redemption
    PaymentStatus can be Partially Paid or Fully Paid. Pendings are usually refunded

    # loop through the dataframe, realise the revenue only if the ticket is present in the attendance
    # RACV height upgrade, revenue is recognized by Roller
    # 477154: 2019/2020 DAY TICKETS are counting RACV height upgrades
    # how to differentiate between General, Junior and Senior?
    '''

    mydf = df[(df['EntryType'] == 'Redemption') & (
            (df['PaymentStatus'] == 'Partially Paid') | (df['PaymentStatus'] == 'Fully Paid'))].copy()
    mydf['timestamp'] = mydf['RedemptionDate'].apply(lambda x: pd.to_datetime(str(x)))
    mydf['dates'] = mydf['timestamp'].dt.date

    # using the weight average to cost out the net reveneue
    w1 = type_admits['2019/2020 DAY TICKETS - RACV Height upgrade']
    w2 = type_admits['2019/2020 RACV - RACV Admission Ticket']
    mydf['CalculatedRevenue'] = mydf.apply(lambda x: party_calculator(x, cost_price_third_party, w1, w2, w1 + w2),
                                           axis=1)

    rdf = mydf.groupby('dates').agg({'TicketId': ['count'], 'CalculatedRevenue': ['sum']}).reset_index()
    rdf.columns = ['dates', 'count', 'revenue']
    rdf['revenue_type'] = 'RACV Tickets'

    return rdf


def excursion_revenue(df, revenue_type):
    '''
    ReceiptNumber is under which the order is stored
    FundsReceived is considered as the payment
    EntryType is Transaction
    PaymentStatus can be Partially Paid or Fully Paid.
    '''

    mydf = df[(df['EntryType'] == 'Transaction') &
              ((df['PaymentStatus'] == 'Partially Paid') | (df['PaymentStatus'] == 'Fully Paid'))].copy()
    mydf['timestamp'] = mydf['RedemptionDate'].apply(lambda x: pd.to_datetime(str(x)))
    mydf['dates'] = mydf['timestamp'].dt.date

    new_df = pd.DataFrame(mydf.groupby(['TicketId', 'dates']).sum()['FundsReceived']).reset_index()

    rdf = new_df.groupby('dates').agg({'TicketId': ['count'], 'FundsReceived': ['sum']}).reset_index()
    rdf.columns = ['dates', 'count', 'revenue']
    rdf['revenue_type'] = revenue_type

    return rdf, new_df


def execute_revenue():
    time_converter = lambda x: dt.datetime.strptime(x, '%d %b %Y %H:%M %p').strftime('%Y-%m-%d')
    admit_frequency = defaultdict(list)
    type_admits = defaultdict()

    for idx, row in df_attn.iterrows():
        admit_frequency[row['Ticket ID']].append(time_converter(row['Checkin Time']))

        if row['Product Name'] in type_admits.keys():
            type_admits[row['Product Name']] += 1
        else:
            type_admits[row['Product Name']] = 1

    # dictionary to see which booking id contains both types of bookings
    xmas_booking = defaultdict(list)

    for idx, row in df_revenue[(df_revenue['ParentProductName'] == '2019/2020 Christmas Festival of Lights') |
                               (df_revenue[
                                    'ParentProductName'] == '2019/2020 Season Pass Discounted Festival of Lights Ticket')].iterrows():
        xmas_booking[row['ReceiptNumber']].append(row['ParentProductName'])

    master_list = [day_revenue(df_revenue[df_revenue['ParentProductName'] == '2019/2020 DAY TICKETS'], 'Day Tickets')]

    test_df, test_df_2 = season_revenue(
        df_revenue[df_revenue['ParentProductName'] == '2019/2020 Season Pass Membership'],
        admit_frequency, 'Season Pass Tickets')

    master_list.append(test_df)

    test_df, test_df_2_season_pass = season_revenue(
        df_revenue[df_revenue['ParentProductName'] == '2019/2020 Season Pass Upgrade'],
        admit_frequency, 'Season Pass Upgrades')

    master_list.append(test_df)  # don't include the count

    master_list.append(group_revenue(df_revenue[df_revenue['ParentProductName'] == '2019/2020 Group Function Tickets']))

    test_df, test_df_2 = christmas_revenue(
        df_revenue[(df_revenue['ParentProductName'] == '2019/2020 Christmas Festival of Lights') |
                   (df_revenue['ParentProductName'] == '2019/2020 Season Pass Discounted Festival of Lights Ticket')],
        xmas_booking)

    master_list.append(test_df)

    third_party_product = ['RACV Height upgrade', '2019/2020 RACV']

    master_list.append(party_revenue(df_revenue[(df_revenue['ProductName'] == third_party_product[0]) |
                                                (df_revenue['ParentProductName'] == third_party_product[1])],
                                     cost_price_third_party, type_admits))

    master_list.append(day_revenue(df_revenue[df_revenue['ParentProductName'] == '2019/2020 Online Agent Tickets'],
                                   'Online Tickets'))

    test_df, test_df_2 = excursion_revenue(
        df_revenue[df_revenue['ParentProductName'] == '2019/2020 School Excursion Tickets'],
        'Excursion Tickets')

    master_list.append(test_df)

    # version 2.0

    master_list.append(day_revenue(df_revenue[df_revenue['ParentProductName'] == '2019/2020 Bulk Tickets'],
                                   'Bulk Tickets'))

    master_list.append(day_revenue(df_revenue[df_revenue['ParentProductName'] == '2019/2020 Corporate Bulk Tickets'],
                                   'Corporate Bulk Tickets'))

    master_list.append(
        day_revenue(df_revenue[df_revenue['ParentProductName'] == '2019/2020 Complimentary Admission Ticket'],
                    'Complimentary Tickets'))

    master_list.append(
        day_revenue(df_revenue[df_revenue['ParentProductName'] == '2019/2020 Holiday Program'],
                    'Holiday Program Tickets'))

    return master_list, test_df_2_season_pass


# adding the dataframes together
master_list, test_df_2_season_pass = execute_revenue()
master_df = master_list[0]

for df in master_list[1:]:
    master_df = master_df.append(df)

master_df.to_csv('results/revenue_report.csv', index=False)
test_df_2_season_pass.to_csv('results/admit_frequency.csv', index=False)
print("FINISHED")
