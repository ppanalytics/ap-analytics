import numpy as np
import pandas as pd
import datetime
from sqlalchemy import create_engine

import cost_analysis as ca
import revenue_recognition as rr

from setup import df_park_usage, df_office_usage, df_rate, df_wages, sal_offset
from setup import third_party_cost_table

from config import connstr
from data_ingestor import df_rev, df_attn, df_kounta, df_kronos


def createCostData():
    """

    :return:
    :rtype:
    """
    wages = ca.wagesCost(df_kronos, sal_offset)
    # utility = ca.utilityCost(rates=df_rate, park=df_park_usage, office=df_office_usage)
    food_cost = ca.foodCost(df_kounta)

    wages.to_csv("results/wage_costs.csv", index=False)
    # utility.to_csv("results/utility_costs.csv", index=False)
    food_cost.to_csv("results/food_cost.csv", index=False)

    return wages, food_cost


def createRevenueData():
    """

    :return:
    :rtype:
    """
    food_revenue = rr.foodRevenue(df_kounta)
    food_revenue.to_csv("results/food_revenue.csv", index=False)

    # merch_revenue = rr.merchRevenue(df_rev)
    # merch_revenue.to_csv("results/merch_revenue.csv", index=False)

    # df_revenue, df_attendance = rr.formatInputData(df_rev, df_attn)
    df_revenue, df_attendance = rr.formatInputData(df_rev, df_attn)
    third_party_cost = third_party_cost_table

    # Parent Product Names:
    day_tickets = "2020/2021 Day Tickets"
    day_tickets_upgrade = "2020/2021 Day Ticket Upgrades"
    season_pass = "2020/2021 Season Pass"
    season_pass_upgrade = "2020/2021 Season Pass Upgrade"
    group_tickets = "2019/2020 Group Function Tickets"
    group_tickets_2 = "2020/2021 Group Function Tickets"
    cl_1 = "2020/2021 Christmas Festival of Lights"
    cl_2 = "2020/2021 Complimentary Christmas Festival of Lights Tickets"
    cl_upgrade = "2020/2021 Christmas Festival of Lights Upgrades - JA to GA"
    tpp = "2020/2021 RACV Tickets"

    online = "2019/2020 Online Agent Tickets"
    online_2 = "2020/2021 Online Agent Tickets"
    excursion = "2020/2021 School Excursion Tickets"
    bulk = "2019/2020 Bulk Tickets"
    corp_bulk = "2018/2019 Corporate Bulk Tickets"
    corp_bulk_2 = "2020/2021 Corporate Bulk Tickets"
    comp_tickets = "2020/2021 Complimentary Admission Tickets"
    comp_tickets_2 = "2020/2021 Complimentary Season Passes"
    holiday = "2020/2021 Holiday Program"

    admin = "2020/2021 Administration Fees"
    cabana = "2020/2021 Cabanas (Off Peak)"
    cabana_2 = "2020/2021 Cabanas (Peak)"
    catering = "2020/2021 Catering"
    birthday = "2020/2021 Deluxe Birthday Package"
    birthday_2 = "2020/2021 Standard Birthday Package"

    winter_glow = "2021 Winter Glow Tickets"
    winter_birthday = "2021 Winter Glow Deluxe Birthday Package"

    # AGGREGATE TICKETS (Group, Online, Bulk, Corporate Bulk, Complimentary, Holiday, Day Revenue, Height Upgrade
    # tickets)
    group_rev = rr.aggregateRevenue(
        rr.multipleformatRevenue(
            df_revenue, group_tickets, group_tickets_2, "Redemption"
        ),
        "Group Tickets",
    )
    online_rev = rr.aggregateRevenue(
        rr.multipleformatRevenue(df_revenue, online, online_2, "Redemption"),
        "Online Tickets",
    )
    bulk_rev = rr.aggregateRevenue(
        rr.formatRevenue(df_revenue, bulk, "Redemption"), "Bulk Tickets"
    )
    complimentary_rev = rr.aggregateRevenue(
        rr.multipleformatRevenue(
            df_revenue, comp_tickets, comp_tickets_2, "Redemption"
        ),
        "Complimentary Tickets",
    )
    corp_bulk_rev = rr.aggregateRevenue(
        rr.multipleformatRevenue(df_revenue, corp_bulk, corp_bulk_2, "Redemption"),
        "Corporate Bulk Tickets",
    )
    holiday_rev = rr.aggregateRevenue(
        rr.formatRevenue(df_revenue, holiday, "Redemption"), "Holiday Program Tickets"
    )
    cabana_rev = rr.aggregateRevenue(
        rr.multipleformatRevenue(df_revenue, cabana, cabana_2, "Redemption"),
        "Cabana Tickets",
    )
    admin_rev = rr.aggregateRevenue(
        rr.formatRevenue(df_revenue, admin, "Redemption"), "Administration Fees"
    )
    catering_rev = rr.aggregateRevenue(
        rr.formatRevenue(df_revenue, catering, "Redemption"), "Catering Tickets"
    )
    birthday_rev = rr.aggregateRevenue(
        rr.multipleformatRevenue(df_revenue, birthday, birthday_2, "Redemption"),
        "Birthday Tickets",
    )
    winter_glow_rev = rr.aggregateRevenue(
        rr.multipleformatRevenue(
            df_revenue, winter_glow, winter_birthday, "Redemption"
        ),
        "Winter Glow Tickets",
    )

    # DAY TICKETS
    day_rev = rr.aggregateRevenue(
        rr.formatRevenue(
            df_revenue,
            day_tickets,
            "Redemption",
        ),
        "Day Tickets",
    )
    day_rev_upgrade = rr.aggregateRevenue(
        rr.formatRevenue(
            df_revenue,
            day_tickets_upgrade,
            "Redemption",
        ),
        "Day Ticket Upgrades",
    )

    # SEASON TICKETS
    season_pass_rev = rr.seasonRevenue(
        df_attendance,
        rr.formatRevenue(df_revenue, season_pass, "Redemption"),
        "Season Pass Tickets",
    )
    season_upg_rev = rr.seasonRevenue(
        df_attendance,
        rr.formatRevenue(df_revenue, season_pass_upgrade, "Redemption"),
        "Season Pass Upgrades",
    )

    # CHRISTMAS REVENUE
    christmas_rev = df_revenue.query(
        "parentproductname == @cl_1 | parentproductname == @cl_2"
    ).query('eventtype == "Redemption"')

    christmas_rev = rr.christmasRevenue(
        christmas_rev, "Christmas Night Lights Tickets", cl_1, cl_2
    )

    christmas_rev_upg = rr.aggregateRevenue(
        rr.formatRevenue(
            df_revenue,
            cl_upgrade,
            "Redemption",
        ),
        "Christmas Night Lights Upgrades",
    )

    # THIRD PARTY BOOKING

    weight = 0.992
    third_party = rr.formatRevenue(df_revenue, tpp, "Redemption")

    third_party_cost = (
        third_party_cost.query('ParentProductName == "RACV"')
        .filter(["ParentProductName", "General Admission", "Junior Admission"])
        .reset_index()
    )
    third_party_cost["ParentProductName"] = tpp
    third_party_cost.columns = [
        "parentproductname",
        "generaladmission",
        "junioradmission",
    ]

    third_party = third_party.merge(third_party_cost, how="left")
    third_party["netrevenue"] = third_party["generaladmission"] * weight + third_party[
        "junioradmission"
    ] * (1 - weight)

    third_party = rr.aggregateRevenue(third_party, "RACV Tickets")

    # EXCURSION TICKETS
    # excursion_rev = df_revenue.assign(NetRevenue=df_revenue["fundsreceived"])
    # excursion_rev = rr.formatRevenue(excursion_rev, excursion, "Transaction")
    #
    # excursion_rev = pd.DataFrame(
    #     excursion_rev.groupby(["ticketid", "dates", "hour"]).sum()["netrevenue"]
    # ).reset_index()
    # excursion_rev = rr.aggregateRevenue(excursion_rev, "Excursion Tickets")
    excursion_rev = rr.aggregateRevenue(
        rr.formatRevenue(
            df_revenue,
            excursion,
            "Redemption",
        ),
        "Excursion Tickets",
    )

    # COMBINE LIST
    master_list = group_rev.append(
        [
            online_rev,
            bulk_rev,
            corp_bulk_rev,
            complimentary_rev,
            holiday_rev,
            season_pass_rev,
            season_upg_rev,
            day_rev,
            day_rev_upgrade,
            christmas_rev,
            christmas_rev_upg,
            third_party,
            excursion_rev,
            cabana_rev,
            catering_rev,
            winter_glow_rev,
            birthday_rev
        ]
    )

    master_list = rr.finalAggregate(master_list)
    master_list.to_csv("results/revenue_report_2.csv", index=False)

    return master_list, food_revenue


def processor():
    """
    function to process data and create a master dataframe

    :return:
    :rtype:
    """
    Start_time = datetime.datetime.now().replace(microsecond=0)
    print("Processing Started: ", Start_time)
    df_wages, df_kounta_cost = createCostData()
    df_breakeven_rev, df_kounta_rev = createRevenueData()
    End_time = datetime.datetime.now().replace(microsecond=0)
    print("Processing Ended: ", End_time)
    print("Time Taken", End_time - Start_time)

    # writing to redshift
    engine = create_engine(connstr)

    df_breakeven_rev.to_sql(
        name="revenue",
        con=engine,
        schema="breakeven",
        if_exists="replace",
        index=False,
    )

    df_kounta_rev.to_sql(
        name="kountarev",
        con=engine,
        schema="breakeven",
        if_exists="replace",
        index=False,
    )

    df_wages.to_sql(
        name="wages",
        con=engine,
        schema="breakeven",
        if_exists="replace",
        index=False,
    )

    df_kounta_cost.to_sql(
        name="kountacost",
        con=engine,
        schema="breakeven",
        if_exists="replace",
        index=False,
    )
    pass


# Run The Processor Function
processor()
