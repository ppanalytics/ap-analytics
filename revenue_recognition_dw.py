"""
This file is to attribute ticket revenue based on different revenue buckets
"""

import pandas as pd
import numpy as np
from collections import defaultdict
import re
import datetime as dt
import time


# TICKET REVENUE


def aggregateRevenue(df, revenue_type):
    """
    aggregates a dataframe by dates and summarises the Revenue

    :param df: needs dates, revenue and count
    :type df: dataframe
    :param revenue_type:
    :type revenue_type:
    :return:
    :rtype:
    """

    df = (
        df.groupby(["dates", "hour"])  # added as part of a code update
            .agg({"TicketId": ["count"], "NetRevenue": ["sum"]})
            .reset_index()
    )

    df.columns = ["dates", "hour", "count", "revenue"]

    df["revenue_type"] = revenue_type

    return df


def seasonRevenue(df_attn, df_revenue, revenue_type):
    """

    :param df_attn:
    :type df_attn:
    :param df_revenue:
    :type df_revenue:
    :param revenue_type:
    :type revenue_type:
    :return:
    :rtype:
    """
    df_revenue = df_revenue.filter(["TicketId", "NetRevenue"])

    visits = df_attn.assign(CheckinDate=df_attn["Checkin Time"].dt.date,
                            CheckinHour=df_attn["Checkin Time"].dt.hour).assign(count=1)
    visits_count = visits.groupby(["Ticket ID"]).agg({"count": ["sum"]}).reset_index()
    visits_count.columns = ["Ticket ID", "count"]
    visits = visits.filter(["Ticket ID", "CheckinDate", "CheckinHour"]).merge(visits_count, how="left")
    visits.columns = ["TicketId", "dates", "hour", "count"]
    visits = visits.merge(df_revenue, how="inner")

    visits["NetRevenue"] = visits["NetRevenue"] / visits["count"]

    tdf = aggregateRevenue(visits, revenue_type)

    return tdf


def receiptsUsed(df):
    """

    :param df:
    :type df:
    :return:
    :rtype:
    """
    receipts_count = (
        df.groupby(["ReceiptNumber", "ParentProductName"])
            .agg({"ReceiptNumber": ["count"]})
            .reset_index()
    )
    receipts_count.columns = ["ReceiptNumber", "ParentProductName", "count"]
    receipts_count = (
        receipts_count.groupby("ReceiptNumber")
            .agg({"ReceiptNumber": ["count"]})
            .reset_index()
    )
    receipts_count.columns = ["ReceiptNumber", "count"]

    return receipts_count


def revenueCheck(df, cl_1, cl_2):
    """

    :param df:
    :type df:
    :param cl_1:
    :type cl_1:
    :param cl_2:
    :type cl_2:
    :return:
    :rtype:
    """
    df["NetRevenue"] = df[
        ["count", "ParentProductName", "NetRevenue", "TaxPayable", "DeferredRevenue"]
    ].apply(
        lambda x: x[2]
        if (x[0] > 1 or x[1] == cl_2 or (x[2] + x[3] + x[4] == 0))
        else (x[2] + x[3] + x[4]) * 0.9,
        axis=1,
    )

    return df


def christmasRevenue(df_revenue, revenue_type, cl_1, cl_2):
    """

    :param df_revenue:
    :type df_revenue:
    :param revenue_type:
    :type revenue_type:
    :param cl_1:
    :type cl_1:
    :param cl_2:
    :type cl_2:
    :return:
    :rtype:
    """
    receipts_count = receiptsUsed(df_revenue)
    df_revenue = df_revenue.merge(receipts_count, how="left")
    df_revenue = revenueCheck(df_revenue, cl_1, cl_2)

    df_final = aggregateRevenue(df_revenue, revenue_type)

    return df_final


def formatRevenue(df, PPName, EType):
    """

    :param df:
    :type df:
    :param PPName:
    :type PPName:
    :param EType:
    :type EType:
    :return:
    :rtype:
    """
    df = df.query("ParentProductName == @PPName").query("EntryType == @EType")

    return df


def formatInputData(df_revenue, df_attn):
    """

    :param df_revenue:
    :type df_revenue:
    :param df_attn:
    :type df_attn:
    :return:
    :rtype:
    """
    # Formatting Revenue df
    df_revenue = df_revenue.assign(
        dates=lambda x: pd.to_datetime(x.RedemptionDate, format="%d %b %Y %H:%M %p")
    ).query('PaymentStatus == "Partially Paid" | PaymentStatus == "Fully Paid" ')
    # df_revenue = df_revenue.assign(dates=df_revenue["dates"].dt.date)
    # updating the input data
    df_revenue = df_revenue.assign(dates=df_revenue["dates"].dt.date, hour=df_revenue["dates"].dt.hour)

    df_attn["Checkin Time"] = pd.to_datetime(
        df_attn["Checkin Time"], format="%d %b %Y %H:%M %p", errors="coerce"
    )

    return df_revenue, df_attn


def finalAggregate(df):
    """

    :param df:
    :type df:
    :return:
    :rtype:
    """
    df = (
        df.groupby(["dates", "hour", "revenue_type"])
            .agg({"count": ["sum"], "revenue": ["sum"]})
            .reset_index()
    )

    df.columns = ["dates", "hour", "revenue_type", "count", "revenue"]

    return df


# FOOD REVENUE


def foodRevenue(df):
    """

    :param df:
    :type df:
    :return:
    :rtype:
    """
    df = df.assign(Date=lambda x: pd.to_datetime(x.Date, format="%d/%m/%Y %H:%M"))
    df = df.assign(Date=df["Date"].dt.date, Hour=df["Date"].dt.hour)

    df_agg = (
        df.groupby(["Date", "Hour", "Outlet"])
            .agg({"Revenue": ["sum"], "Quantity": ["sum"]})
            .reset_index()
    )

    df_agg.columns = ["Date", "Hour", "Outlet", "Revenue", "Quantity"]
    return df_agg


# MERCH REVENUE


def merchRevenue(df):
    """

    :param df:
    :type df:
    :return:
    :rtype:
    """
    df_merch = df[df["ParentProductName"].str.contains("(MERCH|Merch)", regex=True)]
    df_merch = df_merch.query(
        "EntryType == 'Expiration'|EntryType == 'Redemption' "
    ).assign(
        dates=lambda x: pd.to_datetime(x.TransactionDate, format="%d %b %Y %H:%M %p")
    )
    # updating the input data
    df_merch = df_merch.assign(dates=df_merch["dates"].dt.date, hour=df_merch["dates"].dt.hour)

    df_merch = aggregateRevenue(df_merch, "Merch")

    return df_merch
