import pandas as pd
from sqlalchemy import create_engine

from config import connstr

engine = create_engine(connstr)

with engine.connect() as conn, conn.begin():
    # df_kronos = pd.read_sql(
    #     """
    #        select
    #          *
    #        from kronos.employees;""",
    #     conn,
    # )

    df_park_usage = pd.read_sql(
        """
              select
                *
              from origin.park_usage;""",
        conn,
    )

    df_office_usage = pd.read_sql(
        """
              select
                *
              from origin.office_usage;""",
        conn,
    )

    # df_bookings = pd.read_sql(
    #     """
    #           select
    #             *
    #           from roller.bookingitems;""",
    #     conn,
    # )
    #
    # df_products = pd.read_sql(
    #     """
    #           select
    #             *
    #           from roller.products;""",
    #     conn,
    # )
    #
    df_revenue = pd.read_sql(
        """
              select
                *
              from roller.revenues;""",
        conn,
    )

    df_attn = pd.read_sql(
        """
              select
                *
              from roller.attendances;""",
        conn,
    )

    df_products = pd.read_sql(
        """
              select
                *
              from roller.products;""",
        conn,
    )