"""
This file is to attribute ticket revenue based on different revenue buckets
"""

import pandas as pd
import numpy as np
from collections import defaultdict
import re
import datetime as dt
import time


# TICKET REVENUE


def aggregateRevenue(df, revenue_type):
    """
    aggregates a dataframe by dates and summarises the Revenue

    """

    df = (
        df.groupby(["dates", "hour"])  # added as part of a code update
        .agg({"ticketid": ["count"], "netrevenue": ["sum"]})
        .reset_index()
    )

    df.columns = ["dates", "hour", "count", "revenue"]

    df["revenue_type"] = revenue_type

    return df


def seasonRevenue(df_attn, df_revenue, revenue_type):
    """ """
    df_revenue = df_revenue.filter(["ticketid", "netrevenue"])

    visits = df_attn.assign(
        CheckinDate=df_attn["checkindatetime"].dt.date,
        CheckinHour=df_attn["checkindatetime"].dt.hour,
    ).assign(count=1)
    visits_count = visits.groupby(["ticketid"]).agg({"count": ["sum"]}).reset_index()
    visits_count.columns = ["ticketid", "count"]
    visits = visits.filter(["ticketid", "CheckinDate", "CheckinHour"]).merge(
        visits_count, how="left"
    )
    visits.columns = ["ticketid", "dates", "hour", "count"]
    visits = visits.merge(df_revenue, how="inner")

    visits["netrevenue"] = visits["netrevenue"] / visits["count"]

    tdf = aggregateRevenue(visits, revenue_type)

    return tdf


def receiptsUsed(df):
    """ """
    receipts_count = (
        df.groupby(["receiptnumber", "parentproductname"])
        .agg({"receiptnumber": ["count"]})
        .reset_index()
    )
    receipts_count.columns = ["receiptnumber", "parentproductname", "count"]
    receipts_count = (
        receipts_count.groupby("receiptnumber")
        .agg({"receiptnumber": ["count"]})
        .reset_index()
    )
    receipts_count.columns = ["receiptnumber", "count"]

    return receipts_count


def revenueCheck(df, cl_1, cl_2):
    """ """
    df["netrevenue"] = df[
        ["count", "parentproductname", "netrevenue", "taxpayable", "deferredrevenue"]
    ].apply(
        lambda x: x[2]
        if (x[0] > 1 or x[1] == cl_2 or (x[2] + x[3] + x[4] == 0))
        else (x[2] + x[3] + x[4]) * 0.9,
        axis=1,
    )

    return df


def christmasRevenue(df_revenue, revenue_type, cl_1, cl_2):
    """ """
    receipts_count = receiptsUsed(df_revenue)
    df_revenue = df_revenue.merge(receipts_count, how="left")
    df_revenue = revenueCheck(df_revenue, cl_1, cl_2)

    df_final = aggregateRevenue(df_revenue, revenue_type)

    return df_final


def formatRevenue(df, PPName, EType):
    """ """
    df = df.query("parentproductname == @PPName").query("eventtype == @EType")

    return df


def multipleformatRevenue(df, PPName, PPName2, EType):
    """ """
    df = df.query("parentproductname == @PPName | parentproductname == @PPName2").query(
        "eventtype == @EType"
    )

    return df


def formatInputData(df_revenue, df_attn):

    # Formatting Revenue df
    df_revenue = df_revenue.assign(dates=lambda x: pd.to_datetime(x.dateforpbi))
    # df_revenue = df_revenue.assign(dates=df_revenue["dates"].dt.date)
    # updating the input data
    df_revenue = df_revenue.assign(
        dates=df_revenue["dates"].dt.date, hour=df_revenue["dates"].dt.hour
    )

    df_attn = df_attn.assign(
        dates=df_attn["checkindatetime"].dt.date,
        hour=df_attn["checkindatetime"].dt.hour,
    )

    return df_revenue, df_attn


def finalAggregate(df):
    """

    :param df:
    :type df:
    :return:
    :rtype:
    """
    df = (
        df.groupby(["dates", "hour", "revenue_type"])
        .agg({"count": ["sum"], "revenue": ["sum"]})
        .reset_index()
    )

    df.columns = ["dates", "hour", "revenue_type", "count", "revenue"]

    return df


# FOOD REVENUE


def foodRevenue(df):
    """

    :param df:
    :type df:
    :return:
    :rtype:
    """
    df = df.assign(Date=lambda x: pd.to_datetime(x.Date, format="%d/%m/%Y %H:%M"))
    df = df.assign(Date=df["Date"].dt.date, Hour=df["Date"].dt.hour)

    df_agg = (
        df.groupby(["Date", "Hour", "Outlet"])
        .agg({"Revenue": ["sum"], "Quantity": ["sum"]})
        .reset_index()
    )

    df_agg.columns = ["Date", "Hour", "Outlet", "Revenue", "Quantity"]
    return df_agg


# MERCH REVENUE


def merchRevenue(df):
    """

    :param df:
    :type df:
    :return:
    :rtype:
    """
    df_merch = df[df["ParentProductName"].str.contains("(MERCH|Merch)", regex=True)]
    df_merch = df_merch.query(
        "EntryType == 'Expiration'|EntryType == 'Redemption' "
    ).assign(
        dates=lambda x: pd.to_datetime(x.TransactionDate, format="%d %b %Y %H:%M %p")
    )
    # updating the input data
    df_merch = df_merch.assign(
        dates=df_merch["dates"].dt.date, hour=df_merch["dates"].dt.hour
    )

    df_merch = aggregateRevenue(df_merch, "Merch")

    return df_merch
