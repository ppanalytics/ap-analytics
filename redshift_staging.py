"""
File used to process data from excel and push it to redshift database.
Useful for one off operations such as Origin and Recken
"""

import numpy as np
import pandas as pd
from sqlalchemy import create_engine

from config import connstr, electricity_dict


def read_origin_data():
    df_park_usage = pd.read_csv("data/utility/origin/Park_Usage_Origin.csv")
    df_office_usage = pd.read_csv("data/utility/origin/Office_Usage_Origin.csv")

    df_park_usage["Date"] = pd.to_datetime(df_park_usage["Date"]).dt.date
    df_office_usage["Date"] = pd.to_datetime(df_office_usage["Date"]).dt.date

    return df_park_usage, df_office_usage


def electricity_cost_calculator(df, electricity_dict):
    df["energy_price"] = (
        (df["Peak"] * electricity_dict["energy_peak_rate"])
        + (df["OffPeak"] * electricity_dict["energy_off_peak_rate"])
    ) / 100

    df["network_price"] = df["energy_price"] * 1.377

    df["regulated_price"] = (
        (df["Peak"] + df["OffPeak"]) * (electricity_dict["regulated_charge"])
    ) / 100 + electricity_dict["aemo_frc"]

    df["environmental_price"] = (
        (df["Peak"] + df["OffPeak"]) * (electricity_dict["environmental_charge"])
    ) / 100

    df["metering_retail_charge"] = (
        electricity_dict["metering_charge"] + electricity_dict["retail_charge"]
    )

    df["gst"] = (
        df["energy_price"]
        + df["network_price"]
        + df["regulated_price"]
        + df["environmental_price"]
        + df["metering_retail_charge"]
    ) * (1 / 10)

    df["total_price"] = df["gst"] * 11

    return df.round(2)


def write_to_origin(connstr):
    df_park_usage, df_office_usage = read_origin_data()
    df_park_usage = electricity_cost_calculator(df_park_usage, electricity_dict).copy()
    df_office_usage = electricity_cost_calculator(
        df_office_usage, electricity_dict
    ).copy()

    engine = create_engine(connstr)

    df_park_usage.to_sql(
        name="park_usage",
        con=engine,
        schema="breakeven",
        if_exists="replace",
        index=False,
    )

    df_office_usage.to_sql(
        name="office_usage",
        con=engine,
        schema="breakeven",
        if_exists="replace",
        index=False,
    )

    return "ORIGIN TABLES CREATED"


write_to_origin(connstr)
